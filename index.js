const express = require("express");
const bodyParser = require("body-parser");

const Order = require("./lib/order");
const IPFSOrderPublisher = require("./lib/ipfsOrderPublisher");

const app = express();
app.use(bodyParser.json());

const ipfsOrderPublisher = new IPFSOrderPublisher();

app.post("/api/order", async (req, res) => {
  // create an order object from the request body
  const { stock, price, quantity, side } = req.body;
  const order = new Order(stock, price, quantity, side);
  console.log("Order created: ", order);

  await ipfsOrderPublisher.publishOrder(order);
  res.status(201).json({
    message: "Order created successfully",
    order,
  });
});

app.listen(4000, () => console.log("Listening on port 4000"));
