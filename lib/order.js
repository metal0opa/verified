// create a class Order
class Order {
  constructor(stock, price, quantity, side) {
    this.stock = stock;
    this.price = price;
    this.quantity = quantity;
    this.side = side;
  }
}

module.exports = Order;
