const { randomBytes } = require("crypto");
const { createLogger, format, transports } = require("winston");

//  MerkleDAG class
class MerkleDAG {
  constructor() {
    this.dag = new Map();

    this.logger = createLogger({
      level: "debug",
      format: format.combine(
        format.timestamp({
          format: "YYYY-MM-DD HH:mm:ss",
        }),
        format.json()
      ),
      transports: [
        new transports.File({ filename: "merkle-dag.log" }),
        new transports.Console(),
      ],
    });
  }

  addNode(node) {
    const key = randomBytes(8).toString("hex");
    this.dag.set(key, node);
    // this.logger.debug(`Node added to Merkle DAG with key: ${key}`);
    return key;
  }

  removeNode(key) {
    this.dag.delete(key);
    // this.logger.debug(`Node removed from Merkle DAG with key: ${key}`);
  }
}

module.exports = MerkleDAG;
