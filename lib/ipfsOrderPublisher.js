const dotenv = require("dotenv");
const ipfsClient = require("ipfs-http-client");

const MerkleDAG = require("./merkleDAG");

dotenv.config();

//  IPFSOrderPublisher class
class IPFSOrderPublisher {
  constructor() {
    // Initialize IPFS node
    const node = ipfsClient.create({
      host: "localhost",
      port: 5001,
      protocol: "http",
      apiPath: "/api/v0",
    });
    console.log("IPFS node initialized");

    this.ipfs = node;
    this.merkleDAG = new MerkleDAG();
    this.subscriptions = new Map();
  }

  //   publishOrder method
  async publishOrder(order) {
    const key = this.merkleDAG.addNode(order);
    const topic = `${order.stock}${order.side}`;
    await this.ipfs.pubsub.publish(topic, Buffer.from(key));
    console.log(`Published order to topic: ${topic}`);

    if (!this.subscriptions.has(topic)) {
      this.subscriptions.set(
        topic,
        this.ipfs.pubsub.subscribe(topic, (msg) => {
          const orderKey = msg.data.toString();
          const matchedOrder = this.merkleDAG.dag.get(orderKey);

          if (matchedOrder && matchedOrder.side !== order.side) {
            console.log(
              `Matched Orders: ${order.stock}, ${order.price}, ${order.quantity}, ${order.side}`
            );
            console.log(
              `Matched Orders: ${matchedOrder.stock}, ${matchedOrder.price}, ${matchedOrder.quantity}, ${matchedOrder.side}`
            );

            this.merkleDAG.removeNode(key);
            this.merkleDAG.removeNode(orderKey);
          }
        })
      );
      console.log(`Subscribed to topic: ${topic}`);
    }
  }
}

module.exports = IPFSOrderPublisher;
