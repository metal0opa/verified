# verified

A nodejs application that uses IPFS javascript API to

1. create a merkle DAG of nodes, with each node representing an order (Stock name, Price, Quantity). All orders will be linked to each other with each successive order being the root node that comprises the previous order/node.
2. where, each node representing an order publishes whether it is a BUY or SELL using ipfs pubsub. For example, stock A buy will publish 'stockAbuy' and subscribe to 'stockAsell' at the same time.
3. and whenever an order is matched with the above pubsub mechanism, prints the matched orders on console and removes the order (both buy and sell legs) from the DAG.

I have kept the code as simple as possible, so that it is easy to understand. I have also added comments wherever necessary.

## How to run

1. Clone the repo
2. cd into the repo
3. run command `npm install`
4. run command `ipfs daemon --enable-pubsub-experiment`
5. run command `node ./index.js`

## How to test

1. run command `npx mocha ./test/unit/`
