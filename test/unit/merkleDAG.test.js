const MerkleDAG = require("../../lib/merkleDAG");
const sinon = require("sinon");
const assert = require("assert");

describe("MerkleDAG", () => {
  let dag;

  beforeEach(() => {
    dag = new MerkleDAG();
  });

  afterEach(() => {
    sinon.restore();
  });

  it("should add a node to the Merkle DAG", () => {
    const node1 = { name: "node1", value: 100 };
    const node1Key = dag.addNode(node1);
    assert.match(node1Key, /[a-f0-9]{16}/);
    assert.deepEqual(dag.dag.get(node1Key), node1);
  });

  it("should remove a node from the Merkle DAG", () => {
    const node1 = { name: "node1", value: 100 };
    const node1Key = dag.addNode(node1);
    dag.removeNode(node1Key);
    assert.strictEqual(dag.dag.get(node1Key), undefined);
  });
});
