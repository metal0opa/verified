const assert = require("assert");

const Order = require("../../lib/Order");
const MerkleDAG = require("../../lib/merkleDAG");
const IPFSOrderPublisher = require("../../lib/ipfsOrderPublisher");

describe("Order", function () {
  it("should initialize with correct properties", function () {
    const order = new Order("Tesla", 100, 10, "BUY");
    assert.equal(order.stock, "Tesla");
    assert.equal(order.price, 100);
    assert.equal(order.quantity, 10);
    assert.equal(order.side, "BUY");
  });
});

describe("MerkleDAG", function () {
  it("should add and remove nodes correctly", function () {
    const dag = new MerkleDAG();
    const node = {};
    const key = dag.addNode(node);
    assert.equal(dag.dag.get(key), node);
    dag.removeNode(key);
    assert.equal(dag.dag.has(key), false);
  });
});

describe("IPFSOrderPublisher", function () {
  it("should publish and match orders correctly", function () {
    const publisher = new IPFSOrderPublisher();

    const order = new Order("Tesla", 100, 10, "BUY");
    const order2 = new Order("Tesla", 100, 10, "SELL");

    publisher.publishOrder(order);
    publisher.publishOrder(order2);
    assert.equal(publisher.merkleDAG.dag.size, 2);
  });
});
